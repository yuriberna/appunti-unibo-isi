# Appunti UNIBO - Ingegneria e Scienze Informatiche

### Descrizione

In questa repo verranno caricati tutti gli appunti da me creati nel corso dei 3 anni di laurea triennale all'università di bologna (Corso: Ingegneria e scienze informatiche)

### Disclaimer

Ritengo qusta un'opera di riorganizzazione e ridigitalizzazione più che un riassunto vero e proprio.

Il materiale riportato è basato sulle slide messe a disposizione dai professori; potrebbero essere presenti errori di battitura o di concetto nei riassunti da me fatti, quindi prendete tutto con le pinze.

### Importante

- Gli argomenti dei "riassunti" potrebbero non seguire l'ordine degli argomenti delle slide.

### Indice

- [Database](./Database/README.md)
- [HPC](./HPC/README.md)
- [Ingegneria_sw](./Ingegneria_sw/Riassunto_ingegneria_sw.md)
- [Reti](./Reti/README.md)
- [Reti_2](./Reti_2/Riassunto_reti_2.md)
- [Tec_web](./Tec_web/Riassunto_web.md)
- [Tirocinio](./Tirocinio/README.md)
- [IOT](./IOT/README.md)
- [Ricerca_operativa](./Ricerca_operativa/README.md)
