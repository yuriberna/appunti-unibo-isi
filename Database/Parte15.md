### Parte 15 - B-tree e B+tree

- **Definizione**: un B-tree è un albero (direzionato) a più vie perfettamente bilanciato organizzato a nodi, che corrispondono a blocchi dati (pagine) di disco.
    * Siano g, h > 0. Un B-tree T della classe τ(g (ordine), h (altezza)) ha le seguenti proprietà:
        1. Ogni percorso dalla radice a una foglia ha sempre lunghezza h.
        2. Ogni nodo, a eccezione della radice e delle foglie, ha almeno g+1 figli. La radice o è una foglia (h=1) o ha almeno 2 figli; le foglie non hanno figli.
        3. La radice e ogni nodo intermedio hanno al più 2g+1 nodi figli.

        ![alt text](./res/b-tree-1.png "Esempio b-tree.")

    * Un B-tree è organizzato a nodi (o pagine logiche):
        1. Ogni nodo intermedio o nodo foglia memorizza tra g e 2g chiavi; la radice può contenere da 1 a 2g chiavi e può avere di conseguenza da 0 a 2g+1 puntatori a figli.
        2. Un nodo intermedio con L chiavi (g≤L≤2g) ha L+1 puntatori ad altrettanti nodi figli.
        3. In ogni nodo le chiavi sono memorizzate in ordine crescente.

        ![alt text](./res/b-tree-2.png "Esempio nodi b-tree.")

        ![alt text](./res/nodi-sotto.png "Esempio nodi b-tree.")
- **Ricerca**
    * Il costo di ricerca di un valore di chiave è pari al numero di nodi letti: 1≤C(search)≤h
- **Inserimento di una chiave**
    * L'inserimento di una nuova chiave in un B-tree comporta una ricerca per verificare se essa è già presente nell'albero. Nell'ipotesi di non consentire duplicati (primary index), si procede all'inserimento solo in caso di insuccesso della ricerca. 
    * L'inserimento avviene sempre in una foglia. Si distinguono due casi:
        + **Foglia non piena**: si inserisce la chiave e si riscrive la foglia aggiornata.

            ![alt text](./res/ins-b.png "Esempio inserimento foglia non piena b-tree.")

            ![alt text](./res/ins-b-1.png "Esempio inserimento foglia non piena b-tree.")

        + **Foglia piena**: si attiva un processo di "splitting" che può essere ricorsivo e, nel caso peggiore, propagarsi fino alla radice.

            ![alt text](./res/ins-b-pien.png "Esempio inserimento foglia piena b-tree.")

    * **Gestione dell'overflow**
        + Due nodi P e P* sono adiacenti se figli dello stesso padre Q e indirizzati da puntatori adiacenti in Q.
        + Una strategia per evitare eccessivi split consiste nell'accedere a un nodo adiacente P* al nodo P pieno e ridistribuire le chiavi tra P, P* e Q.
- **Eliminazione di una chiave**
    * Se la chiave y da cancellare si trova in una foglia L, si rimuove.
    * Altimenti y è rimpiazzata dal valore di chiave più piccolo del suo sottoalbero di destra.
    * Se, in conseguenza di ciò, L viene a contenere un numero di chiavi inferiore a g, si rende necessario modificare la struttura dell'albero tramite processi di catenation e underflow.
        + **Catenation**
            - La catenation di due nodi adiacenti P e P’ è possibile se, complessivamente, i due nodi contengono meno di 2g chiavi.
            - Ogni nodo contiene almeno g chiavi, la catenation si attiva quando P ha g-1 chiavi e P’ esattamente g.

            ![alt text](./res/catenation.png "Esempio di catenation.")

        + **Underflow**
            - Se, prima della cancellazione, la somma del numero delle chiavi in P e P’ è maggiore di 2g, allora le chiavi devono essere ridistribuite tra i due nodi e il nodo padre Q.

            ![alt text](./res/underflow.png "Esempio underflow.")

- **Prestazioni di un b-tree**
    * **Notazione**
        + NR = Numero record
        + NP = Numero pagine
        + NK = Numero chiavi
        + IP = Numero nodi indice
        + NL = Numero foglie 
    * **Nodi**
        + **Minimo**: $`IPmin = 1+\frac{2}{g}((g+1)^{h-1}-1)`$
        + **Massimo**: $`IPmax = \frac{1}{2g}((2g+1)^h-1)`$
    * **Chiavi**
        + **Minimo**: $`NKmin = 2((g+1)^{h-1}-1)`$
        + **Massimo**: $`NKmax = (2g+1)^h-1`$
    * **Altezza**: $`\lceil\log_{2g+1}{(NK+1)}\rceil\le h\le\lfloor 1+\log_{g+1}{(\frac{NK+1}{2})}\rfloor`$
    * **Costo di un inserimento**: 
        + **Minimo** (no splitting): $`h+1`$
        + **Massimo** (splitting fino radice): $`3h+1`$
        + **Medio**: $`< h+1+\frac{2}{g}`$
    * **Costo di un'eliminazione**: 
        + **Minimo** (foglia): $`h+1`$
        + **Massimo**: $`3h`$
        + **Medio**: $`< h+5+\frac{3}{g}`$
    * **Ordine**: $`g=\lfloor\frac{D-len(q)}{2(len(k)+len(p)+len(q))}\rfloor`$
    * **Pregi e difetti**
        + **Pro**
            - Molto efficiente per la ricerca e la modifica di singoli record. Ad esempio, con un B-tree di ordine g=146 e NK=$`10^9`$ chiavi, si ha: $`h\le 1+log_{147}(\frac{10^9}{2})=5`$ e dunque la ricerca di una chiave comporta al massimo 5 accessi a disco.
        + **Contro**
            - Non è particolarmente adatto per elaborazioni di tipo sequenziale nell'ordine dei valori di chiave, e nel reperimento di valori di chiave in un intervallo dato.
            - La ricerca del successore di un valore di chiave può comportare la scansione di molti nodi.
            - La ricerca del valore di chiave più piccolo, che si trova nella foglia più a sinistra, implica l'accesso a tutti i nodi del percorso tra la radice e la foglia.

            ![alt text](./res/con-b-tr.png "Esempio ricerca di un intervallo in un b-tree.")

- **B`*`-tree** (Knuth)

    ![alt text](./res/bshtr.png "Differenza fra b-tree e b*-tree.")

    * **Terminologia**
        + **Ordine**: massimo numero m di figli che può avere un nodo (m-1) è il massimo numero di chiavi in un nodo).
        + Ogni nodo, eccetto le foglie, ha al più m figli.
        + Ogni nodo non foglia, a eccezione della radice, ha almeno ⌈m/2⌉ figli.
        + La radice ha almeno 2 figli a meno che non sia una foglia.
        + Un nodo non foglia con k figli contiene k-1 chiavi.
        + Tutte le foglie sono allo stesso livello.

        ![alt text](./res/es-knuth.png "Esempio di b*-tree.")

- **B+tree**
    * Le foglie contengono tutti i valori di chiave.
    * La radice e i nodi interni, organizzati come un B-tree, costituiscono solo una "mappa" per consentire una rapida localizzazione delle chiavi, e memorizzano "separatori" di cammino.
    * A paritàdidimensionedelnodo,l’ordinedelB-treechefungedamappaè  piùgranderispettoa quellodi   unsoloB-tree,nondovendonéla radicenéi nodiinternimemorizzarepuntatoria record.
    * Le foglie sono concatenate in una lista solitamente doppiamente concatenata. È inoltre presente un puntatore alla testa della lista. In alcune implementazioni anche i nodi interni sono collegati in una lista doppiamente concatenata.

    ![alt text](./res/bptree.png "Struttura albero b+tree.")

    ![alt text](./res/nbptree.png "Struttura nodi b+tree.")

    * **Ordine**
        + E' un concetto significativo solo se si fa uso di separatori di lunghezza fissa. Negli altri casi si può considerare la lunghezza media dei separatori stessi.
        + **Forumla**: $`g=\lfloor\frac{D-len(q)}{2(len(k)+len(q))}\rfloor`$
        + Esempio: 
            - D = 4096 byte
            - len(q) = 4 byte
            - len(k) = 10 caratteri
            - g = 146, 4 byte inutilizzati in ogni nodo
    * **Primary B+tree**
        + **Ordine delle foglie**: $`gleaf=\lfloor\frac{D-2len(q)}{2(len(k)+len(p))}\rfloor`$
        + **Numero foglie**
            - $`u=0.69`$
            - $`NL=\lceil\frac{NR\times (len(k)+len(p))}{D\times u}\rceil`$
            - $`NL=\lceil\frac{NR}{2\times gleaf\times u}\rceil`$
        + **Altezza minima**
            - Si ottiene quando tutti i nodi dei livelli intermedi (radice compresa) sono pieni. 
            - $`h\ge 1+\lceil\log_{2g+1}NL\rceil`$
        + **Altezza massima**
            - La radice contiene 2 soli puntatori e ogni nodo dei livelli intermedi ne contiene g+1.
            - $`h\le 2+\lfloor\log_{g+1}\frac{NL}{2}\rfloor`$
        + **Ricerca di valori**
            - **Costo ricerca singolo valore di chiave**: h (altezza albero) + 1 (accesso alla pagina ove risiede il record nel caso in cui i record non sono memorizzati nelle foglie).
            - **Costo ricerca range**
                * Utilizzo le sequenze memorizzate nelle foglie che sono tra loro concatenate.
                * EK = numero valori distinti di chiave nel range.
                * **Costo**: $`CI=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                    + Solo se le foglie contengono i record, altrimenti si  deve stimare il numero di pagine in cui gli EK record sono memorizzati e distinguere tra primary clustered e primary unclustered.
    * **Secondary b+tree** 
        + **RID** (Record Identifier): per ogni valore di chiave si ha nelle foglie una lista di puntatori (RID, detto anche TID: Tuple Identifier) ai record con quel valore ordinata per valori crescenti.

            ![alt text](./res/rid-bptree.png "Organizzazione secondary clustered b+tree con RID.")

            ![alt text](./res/rid-bptree-un.png "Organizzazione secondary unclustered b+tree con RID.")

            - **Numero foglie**: $`NL=\lceil\frac{NK\times len(k)+NR\times len(p)}{D\times u}\rceil`$
                * Una foglia deve essere indirizzata dal livello superiore solo se contiene RID di un "nuovo" valore di chiave. Per calcolare l'altezza è quindi necessario usare min{NK,NL} in luogo di NL.

                    ![alt text](./res/nfoglie-secbptree.png "Dettaglio secondary clustered b+tree con RID.")

        + **PID** (Page Identifier): per un valore di chiave, la lista consiste di tanti PID quante sono le pagine del file dati che contengono almeno un record con quel valore di chiave; è pertanto una soluzione conveniente quando molti record con lo stesso valore di chiave si trovano nella stessa pagina.

            ![alt text](./res/pid-bptree.png "Organizzazione secondary clustered b+tree con PID.") 

            ![alt text](./res/pid-bptree-un.png "Organizzazione secondary unclustered b+tree con PID.") 

        + **Posting file**
            - Area separata in cui sono memorizzate le liste dei RID (o dei PID).
            - Le foglie contengono, per ciascun valore di chiave distinto, un puntatore alla testa della relativa lista. 
            - Al momento dell'inserimento di un nuovo valore di chiave viene allocato un posting record di dimensione dmin contenente il puntatore al record dati associato al valore di chiave.
            - Questo record sarà poi riempito con i successivi puntatori per valori ripetuti della chiave in questione; a spazio esaurito sarà concatenato in lista a un altro nuovo posting record, e così via.

            ![alt text](./res/posting-file.png "Organizzazione posting file.")
    
    * **Esempio riepilogativo**
        - Si consideri una relazione USERS che consiste di:
            * NR=200000 record di dimensione d=160 byte l'uno, organizzati in un file ordinato su UserId.
            * Le pagine hanno dimensione D=4 KB di cui 96 byte utilizzati per l'header.
            * uP=0.85 l'utilizzazione di ogni pagina del file dati.
            * Sono dati due indici; si consideri il caso peggiore con u=0.5:
                + IX(USERS.UserId) su UserId (primary dense clustered)
                + IX(USERS.BirthDate) su BirthDate (secondary dense unclustered)
            * NKBirthDate=4000
            * len(Userid)=10 byte
            * len(BirthDate)=4 byte 
            * len(p)=len(RID)=4 byte
            * len(q)=len(PID)=2 byte
            * D*=D-96=4000 byte 
        - Si ricava che il file dati ha un numero di pagine pari a: $`NP=\lceil\frac{NR}{\lfloor\frac{up\times D*}{d}\rfloor}\rceil =\lceil\frac{200000}{\lfloor\frac{0.85\times 4000}{160}\rfloor}\rceil =9524`$
        - Risoluzione:
            * IX(USERS.UserId)
                + Formule:
                    - $`gleaf=\lfloor\frac{D-2\times len(q)}{2\times (len(k)+len(p))}\rfloor`$
                    - $`NL=\lceil\frac{NR}{2\times gleaf\times u}\rceil`$
                    - $`1+\lceil\log_{2g+1}NL\rceil\le h\le 2+\lfoor\log_{g+1}\frac{NL}{2}\rfloor`$
                + Con:
                    - u=0.5
                    - len(p)=4
                    - len(q)=2
                    - len(k)=len(UserID)=10
                    - NR=NKUserID=200000
                    - Sostituisco a D il valore D*=D−96=4000
                + Ottengo:
                    - $`gleaf(IX(USERS.UserID))=\lfloor\frac{D*-2\times len(q)}{2\times (len(UserID)+len(p))}\rfloor=\lfloor\frac{4000-2\times 2}{2\times (10+4)}\rfloor =142`$
                    - $`g(IX(USERS.UserID))=\lfloor\frac{D*-len(q)}{2\times (len(UserID)+len(q))}\rfloor=\lfloor\frac{4000-2}{2\times (10+2)}\rfloor =166`$
                    - $`NL(IX(USERS.UserID))=\lceil\frac{200000}{2\times 142\times 0.5}\rceil =1409`$
                    - $`h(IX(USERS.UserID))=2+\lfloor\log_{g+1}\frac{NL(IX(USERS.UserID))}{2}\rfloor=2+\lfloor\log_{167}\frac{1409}{2}\rfloor =3`$ (altezza massima)
            * IX(USERS.BirthDate)
                + NKBirthDate=4000 valori distinti
                + NR=200000 record nel file
                + Dunque mediamente 200000/4000=50 RID per valore di chiave devono essere presenti in ogni foglia dell'indice (non si assume un posting file per semplicità)
                + Si ricava:
                    - $`gleaf(IX(USERS.BirthDate))=\lfloor\frac{D*-2\times len(q)}{2\times (len(BirthDate)+50\times len(p))}\rfloor=\lfloor\frac{4000-2\times 2}{2\times (4+50\times 4)}\rfloor =9`$
                        * (Questa formula, per indici secondari, si può applicare solo se un valore di chiave con i relativi RID è contenuto in una sola foglia, altrimenti si deve usare: $`NL=\lceil\frac{NK\times len(k)+NR\times len(p)}{D\times u}\rceil`$)
                + Ora ogni foglia deve poter ospitare al massimo 2*9 valori di chiave ma è utilizzata al 50% nel caso peggiore in considerazione, da cui si ricava:
                    - $`NL(IX(USERS.BirthDate))=\lceil\frac{NK(USERS.BirthDate)}{2\times 9\times 0.5}\rceil =\lceil\frac{4000}{2\times 9\times 0.5}\rceil =445`$
                + Infine si deriva l'altezza massima, calcolando l'ordine g del B-tree che funge da mappa:
                    - $`g=\lfloor\frac{4000-2}{2\times (4+2)}\rfloor =333`$
                    - $`h(IX(USERS.BirthDate))=2+\lfloor\log_{g+1}\frac{NL}{2}\rfloor =2+\lfloor\log_{334}\frac{445}{2}\rfloor =2`$
    * **Convenienza dell'uso di un indice**
        + Benché un B+-tree consenta di eseguire efficacemente ricerche su valori di chiave compresi in un intervallo, nel caso di indici unclustered sono possibili casi in cui è preferibile la scansione sequenziale.
        + Decidere se fare uso di scansione sequenziale o se accedere tramite indice è un'istanza particolare del problema dell'ottimizzazione delle query, il cui scopo è determinare la strategia di esecuzione che sia a costo minimo.
        + Metrica per valutare la bontà di una strategia di esecuzione: minimizzazione del numero di operazioni di I/O.
        + **Modus operandi per valutare la convenienza**
            - **Analisi**
                1. Ordine dei dati in uscita (interessa?)
                2. Ordinamento preventivo dei RID (viene eseguito?)
                    1. Relativi a un valore di chiave
                    2. Relativi a più valori di chiave3
                3. Distribuzione dei valori di chiave (è uniforme?)
                4. Distribuzione dei record con un certo valore di chiave sulle pagine del file dati (è casuale?)
                * Esempio: **Unclustered B+-tree vs sequential scan**
                    1. Non è richiesto nessun ordinamento dei dati in output.
                        + Non devono essere considerati eventuali costi di ordinamento.
                    2. Ordinamento preventivo dei RID
                        1. La lista di RID di un valore di chiave è ordinata o comunque viene ordinata prima di accedere ai dati.
                            + Per reperire, via indice, tutti i record con un valore di chiave, non si accede mai più di una volta alla stessa pagina dati...
                        2. ... ma non si esegue il merge di più liste di RID.
                            + ...ma ciò è possibile nel caso di record con valori di chiave diversi.
                        3. La distribuzione dei valori di chiave sui record è uniforme.
                            + Ogni valore di chiave è ripetuto, in media, NR/NK volte.
                        4. Record con uno stesso valore di chiave sono distribuiti uniformemente (allocati casualmente) sulle pagine del file dati.
                    + Da 2.1 e 3 si deriva che il problema è stimare il numero (medio) di pagine che contengono almeno uno degli NR/NK record aventi un certo valore di chiave.
                    + L'ipotesi 4 è (molto) più forte rispetto a dire semplicemente che il file non è ordinato secondo i valori di chiavi di un certo campo. Di fatto, corrisponde ad affermare che non esiste, rispetto a tali valori, nessuna forma particolare di "clustering" dei record. Da un punto di vista probabilistico, equivale a dire che il campo in esame e quello di ordinamento sono tra loro indipendenti.
        + **Modello di Cardenas**
            - Consente di derivare una formula per stimare il numero medio di pagine, su un totale di NP, che contengono almeno uno degli ER record che si devono reperire.
            - Esempio: Data un'urna con infinite biglie di c (NP) colori, ogni colore ripetuto un infinito numero di volte, quanti colori distinti risultano, in media, dall'estrazione di e (ER) biglie?
            - **Formula**: $`Φ(ER,NP)=NP\times (1-(1-\frac{1}{NP})^{ER})\le min(ER,NP)`$
        + **Costi con indice unclustered**
            - Se si devono reperire EK valori della chiave di ricerca (EK<NK):
                * **Scansione sequenziale**: il costo è pari al numero delle pagine del file dati, dunque Ca(seq)=NP.
                * **Accesso con indice unclustered**: il costo si valuta come la somma di due componenti: C(uncl)=CI(uncl)+CD(uncl).
                * Costo di accesso all'indice: $`CI(uncl)=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * Costo di accesso alle pagine dati: $`CD(uncl)=EK\times Φ(\frac{NR}{NK},NP)`$
                * NB: Per query di set le stime devono essere modificate per tener conto che la ricerca riparte dalla radice per ogni valore della chiave di ricerca.
        + **Costi con indice clustered**
            - Si consideri ora il caso in cui l'indice sia costruito su un attributo (o combinazione di attributi) di ordinamento del file e le foglie contengano puntatori a record. In questo caso non si può usare la formula di Cardenas perché i record non sono allocati casualmente nelle pagine dati.
            - Se si devono reperire EK valori della chiave di ricerca (EK<NK):
                * **Accesso con indice clustered**: il costo si valuta come la somma di due componenti: C(clus)=CI(clus)+CD(clus).
                * Costo di accesso all'indice: $`CI(clus)=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * Costo di accesso alle pagine dati: $`CD(clus)=\lceil\frac{EK}{NK}\times NP\rceil`$
                * NB: Per query di set le stime devono essere modificate per tener conto che la ricerca riparte dalla radice per ogni valore della chiave di ricerca.
        + **Utilizzo congiunto di più indici**
            - Per risolvere interrogazioni complesse in alcuni RDBMS è possibile utilizzare più indici, tramite algoritmi di intersezione e unione di RID (o  PID).

            ![alt text](./res/ind-cong.png "Esempio utilizzo congiunto indici.")

            - NB: Se gli indici sono a PID l'intersezione dei PID può dar luogo a "false drop", ovvero accesso a pagine che non contengono record che soddisfano l'intero predicato.
            - **Indici su combinazioni di attributi**
                * Un indice sulla combinazione ordinata di attributi (A1,A2,... ,An), è un tipo particolare di organizzazione secondaria multi-attributo che memorizza come valori di chiave tutte le combinazioni distinte di valori di tali attributi presenti nel file dati.

                ![alt text](./res/ind-comb-at.png "Esempio indici su combinazioni di attributi.")

                * **Pro**
                    + Riduzione del numero di RID di un fattore n
                    + Efficienza di elaborazione di interrogazioni che specificano condizioni sui primi r attributi (1≤r≤n)
                    + Efficienza nell'aggiornamento
                * **Contro**
                    + Inefficienza nell'elaborazione di interrogazioni che specificano condizioni sugli ultimi (n-r) attributi, a causa dell'assenza di contiguità dei valori di chiave, che può portare a dover leggere tutte le foglie.
                * Esempio (**Bit-mapped indexing**):
                    
                    ![alt text](./res/bm-ind.png "Esempio bit-mapped indexing.")
                
- **Aggiunte**
    * **Primary**
        + **Unclustered**
            - **Predicato di uguaglianza**
                * $`CI=h`$
                * $`CD=1`$
            - **Predicato di range**
                * $`CI=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * $`CI=\lceil EK\rceil`$
            - **Predicato di set**
                * $`CI=EK\times (h-1)+EK\times\lceil\frac{NL}{NK}\rceil`$
                * $`CD=EK`$
        + **Clustered**
            - **Predicato di uguaglianza**
                * $`CI=h`$
                * $`CD=1`$
            - **Predicato di range**
                * $`CI=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * $`CI=\lceil\frac{EK}{NK}NP\rceil`$
            - **Predicato di set**
                * $`CI=EK\times (h-1)+EK\times\lceil\frac{NL}{NK}\rceil`$
                * $`CD=EK\times\lceil\frac{NP}{NK}\rceil`$
    * **Secondary**
        + **Unclustered**
            - **Predicato di uguaglianza**
                * $`CI=h-1+\lceil\frac{1}{NK}NL\rceil`$
                * $`CD=Φ(\frac{NR}{NK},NP)`$
            - **Predicato di range**
                * $`CI=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * $`CI=EK\times Φ(\frac{NR}{NK},NP)`$
            - **Predicato di set**
                * $`CI=EK\times (h-1)+EK\times\lceil\frac{NL}{NK}\rceil`$
                * $`CI=EK\times Φ(\frac{NR}{NK},NP)`$
        + **Clustered**
            - **Predicato di uguaglianza**
                * $`CI=h-1+\lceil\frac{1}{NK}NL\rceil`$
                * $`CD=\frac{NP}{NK}`$
            - **Predicato di range**
                * $`CI=h-1+\lceil\frac{EK}{NK}NL\rceil`$
                * $`CI=\lceil\frac{EK}{NK}NP\rceil`$
            - **Predicato di set**
                * $`CI=EK\times (h-1)+EK\times\lceil\frac{NL}{NK}\rceil`$
                * $`CI=EK\times\lceil\frac{NP}{NK}\rceil`$