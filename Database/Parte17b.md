### Parte 17 b - Query plan cardinality and histogram selectivity

- **Costi piani d'accesso**
    * **Full table scan**: $`C(seqR) = C_{I/O}(seqR) + C_{CPU}(seqR) = NP_{R} + α \times NR_{R}`$
    * **IX(R.A) index scan**: $`C(IX(R.A){uncl|clus}) = C_{I/O}(IX(R.A){uncl|clus}) + C_{CPU}(IX(R.A){uncl|clus}) = EN_{p(R.A)} + EL_{p(R.A)} + EP_{p(R.A)} + α \times ER_{R,p(R.A)}`$
        + Sia clustered che unclustered:
            - $`EN_{p(R.A)} = h_{IX(R.A)} - 1`$ per query su singolo valore di range
            - $`EN_{p(R.A)} = (h_{IX(R.A)} - 1) \times EK_{p(R.A)}`$ per query di set nel caso peggiore
            - $`EL_{p(R.A)} = \lceil f_{p(R.A)} \times NL_{IX(R.A)} \rceil`$ per query su singolo valore di range
            - $`EL_{p(R.A)} = EK_{p(R.A)} \times \lceil \frac{NL_{IX(R.A)}}{NK_{A}} \rceil`$ per query di set nel caso peggiore
        + $`EP_{p(R.A)} = EK_{p(R.A)} \times Φ(\frac{NR_{R}}{NK_{R.A}}, NP_{R})`$ per indice unclustered
        + $`EP_{p(R.A)} = \lceil f_{p(R.A)} \times NP_{R} \rceil`$ per indice clustered e query su singolo valore o di range
        + $`EP_{p(R.A)} = EK_{p(R.A)} \times \lceil \frac{NP_{R}}{NK_{R.A}} \rceil`$ per indice clustered e query di set nel caso peggiore
    * **Note**
        + Nel caso di indice clustered come organizzazione primaria ad albero che memorizza direttamente nelle foglie i record dati, le formule prima introdotte si semplificano perché sparisce il termine $`EP_{p(R.A)}`$ che viene a coincidere con $`EL_{p(R.A)}`$.
        + Per il caso di $`EK_{p(R.A)} = NK_{A}`$ le formule per l'accesso con indice si  semplificano perché si reperiscono tutti i valori di chiave, e dunque tutti i record. Si assume di disporre del puntatore alla prima foglia dell'indice. Le formule diventano:
            - IX(R.A) index scan:
                * C(IX(R.A)uncl) = $`NL_{IX(R.A)} + NK_{R.A} \times Φ(\frac{NR_{R}}{NK_{R.A}, NP_{R}) + α \times NR_{R}`$ 
                    + NB: Il costo può essere molto oneroso ma può essere valutato, in certi casi, confrontandolo con un ordinamento della relazione.
                * C(IX(R.A)clus) =
                    + $`NP_{R} + α \times NR`$ scansione sequenziale della relazione ordinata
                    + $`NL_{IX(R.A)} + α \times NR_{R}`$ se le foglie contengono direttamente i record dati
    * Esempi
        + Esempio 1
        
            ![alt text](./res/es-cost-query-pl.png "Esempio di query cost.")

            ![alt text](./res/es-query-cost-1.png "Esempio di query cost.")
        
        + Esempio 2

            ![alt text](./res/es-query-cost-2.png "Esempio di query cost.")
        
    * **Ordinamento del risultato**
        + Se viene richiesto un ordinamento del risultato (ORDERBY o GROUPBY) si devono considerare:
            - il fattore di selettività della query che consente di stimare il numero di record in uscita
            - la dimensione in byte del record nel risultato note le dimensioni dei singoli attributi citati nella "select list"
            - il criterio di ordinamento
        + Se il metodo d'accesso fornisce già il risultato nell'ordine voluto non vi è un ulteriore costo da considerare altrimenti:
            - numero di pagine per contenere il risultato: $`EP_{R,q} = \lceil \frac{ER_{R,q} \times L_{R,q}}{D_{R}*} \rceil`$ 
            - numero di operazioni di I/O del sort-merge a Z vie: $`C_{I/O}(sort(EP_{R,q})) = 2 \times EP_{R,q} \times \lceil 1 + log_{z} \lceil \frac{EP_{R,q}}{(Z + 1) \times FS} \rceil \rceil`$ assumendo di avere un'area buffer di Z+1 frame di dimensione FS pagine ciascuno: B=(Z+1)×FS
        + **Note**
            - La formula precedente, valida nel caso in cui si operi per materializzazione, assume di usare B=(Z+1)×FS pagine per la fase di sort interno sia per input sia per output; in fase di merge si utilizzano Z×FS pagine per input e FS pagine per output. Spesso si assume FS=1 e Z=B-1 e si usa l'approssimazione: $`C_{I/O}(sort(EP_{R,q})) = 2 \times EP_{R,q} \times \lceil log_{z}EP_{R,q} \rceil`$
            - Se $`EP_{R,q} \le B`$, ovvero l'ordinamento può essere eseguito in memoria centrale, non vi è né un costo di lettura né di scrittura di pagine, e dunque si deve porre $`C_{I/O}(sort(EP_{R,q})) = 0`$
            - In pipeline il costo si riduce a $`C_{I/O}(sort(EP_{R,q})) = 2 \times EP_{R,q} \times \lceil log_{z} \lceil \frac{EP_{R,q}}{(Z+1) \times FS} \rceil \rceil`$
            - Per il passo di sort interno l'ordinamento avviene a B=(Z+1)×FS pagine alla volta: $`C_{CPU}(intSort(EP_{R,q})) = α \times \lceil \frac{EP_{r,q}}{B} \rceil \times \lceil ER_{B} \times log_{2} ER_{B} \rceil`$  con $`\lceil \frac{EP_{r,q}}{B} \rceil`$ numero di run ordinate prodotte e con $`ER_{B} = \frac{ER_{R,q} \times B}{EP_{R,q}}`$ numero medio di record risultato che l'area di buffer può ospitare.
            - Per la fase di merge (solo se è necessaria ovvero se $`EP_{R,q} \g B`$) si devono considerare i vari passi di fusione e per ciascuno stimare il numero di confronti; se si usa un binary heap per produrre l'output una possibile stima è: $`C_{CPU}(intMerge(EP_{R,q})) = α \times log_{z} ( \lceil \frac{EP_{r,q}}{B} \rceil ) \times \lceil ER_{R,q} \times log_{2} Z \rceil`$
            - Se $`EP_{R,q} \g B`$ il costo totale in termini di CPU è: $`C_{CPU}(sort(EP_{R,q})) = C_{CPU}(intSort(EP_{R,q})) + C_{CPU}(intMerge(EP_{R,q}))`$
            - $`C(sort(EP_{R,q})) = C_{I/O}(sort(EP_{R,q})) + C_{CPU}(sort(EP_{R,q}))`$
        + Esempi
            - Esempio 3

                ![alt text](./res/es3-costi.png "Esempio di costi.")

                ![alt text](./res/es3-1-costi.png "Esempio di costi.")

            - Esempio 4

                ![alt text](./res/es4-costi.png "Esempio di costi.")
            
    * **Selezione con disgiunzioni**
        + In presenza di query di selezione senza disgiunzioni sono piani di accesso candidati quelli che usano condizioni in AND risolvibile con indici, singolarmente o in abbinamento ad altri con l'esecuzione di RID intersection.
        + Se la condizione espressa da WHERE è una disgiunzione ed esiste anche una sola condizione non risolvibile con indice, occorre scandire l'intera tabella.
        + Se esiste almeno una condizione in AND risolvibile con indice, si usa il percorso più efficiente.
        + Se tutte le condizioni in OR sono risolvibili con indice, è possibile far ricorso agli indici sui predicati citati nella disgiunzione ed eseguire unione dei RID.
        + Esempio
            ```
            SELECT      *
            FROM        EMPLOYEES E
            WHERE       DeptNo BETWEEN 50 AND 100
            AND         (Job=‘Manager’ OR Salary > 40000)
            ORDER BY    Salary
            ```
            - Se sono disponibili indici IX(E.Job) e IX(E.Salary), ed è possibile usare più indici alla volta per seguire la query, deve essere valutato un piano d'accesso che li contempla entrambi ed esegue RID union. Se esiste anche un indice su IX(E.DeptNo) è anche possibile un piano d'accesso che usa i tre indici, eseguendo prima RID union per i primi due indici IX(E.Job) e IX(E.Salary), e poi RID intersection con IX(E.DeptNo). L'uso del solo indice IX(E.DeptNo) o del full scan sono altri due possibili piani.

- **Proiezione**
    * Y={R.A1, R.A2,...,R.An} l'insieme degli attributi citati nella select list. La cardinalità del risultato della proiezione si può stimare come:
        + $`ER_{πY(R)}=NR_{R}`$ se Y contiene una chiave candidata
        + $`ER_{πY(R)}=NK_{R.A}`$ se la proiezione riguarda un solo attributo A
        + $`ER_{πY(R)}=min(NR_{R},∏_{i}NK_{R.Ai})`$ (si assume indipendenza tra gli attributi)
    * In modo equivalente si può definire il fattore di selettività di $`π_{Y}(R)`$:
        + $`f_{πY(R)} = 1`$ se Y contiene una chiave candidata
        + $`f_{πY(R)} = \frac{NK_{A}}{NR}`$ se la proiezione riguarda un solo attributo A
        + $`f_{πY(R)} = \frac{min(NR_{R},∏_{i}NK_{R.Ai})}{NR_{R}}`$ (si assume indipendenza tra gli attributi)
    * **Metodi di esecuzione**
        + In assenza di altre condizioni di selezione l'esecuzione consiste nella scansione della relazione con un costo pari a: $`C(seqR) = NP_{R} + α \times NR_{R}`$
        + Se la select list non include una chiave candidata allora è necessario provvedere all'eliminazione di duplicati e sono possibili tre metodi d'esecuzione:
            - **Sorting**
                1. Si esegue un full scan di R e si produce una tabella T che contiene solo gli attributi richiesti nella select list: $`C(seq R & build T) = NP_{R} + α \times NR_{R} + EP_{T}`$
                2. Sia $`EP_{T}`$ il numero stimato di pagine di T e sia $`NR_{T} = NR_{R}`$ il numero di record di T. Si ordina T usando la combinazione lessicografica di tutti gli attributi:
                    * $`C_{I/O}(sort(EP_{T})) = C_{I/O}(sort(EP_{T})) + C_{CPU}(sort(EP_{T}))`$
                    * $`C_{I/O}(sort(EP_{T})) = 2 \times EP_{T} \times \lceil 1 + log_{Z} \lceil \frac{EP_{T}}{(Z+1) \times FS} \rceil \rceil`$
                    * $`C_{I/O}(sort(EP_{T})) = α \times \frac{EP_{T}}{B} \times \lceil ER_{B} \times log_{2}ER_{B} \rceil + α \times log_{Z}(\lceil \frac{EP_{T}}{B} \rceil) \times \lceil NR_{T} \times log_{2}Z \rceil`$
                3. Si scandisce T eliminando i duplicati: $`C(seq T) = EP_{T} + α \times NR_{T}`$
            - **Hashing**
            - **Indice**
- **Join**
    * $`f_{FJ(R,S)}`$: fattore di selettività di join senza predicati locali
        + Theta join: $`f_{FJ(R,S)} = \frac{ER}{NR_{R} \times NR_{S}}`$
        + Equi join: $`f_{p(R.J=S.J)} = \frac{1}{max(NK_{R.J}, NK_{S.J})}`$
        + Equi join su primary key e foreign key: $`f_{p(R.J=S.J)} = \frac{1}{NK_{R.J}}`$
        + Equi join m:n: $`f_{p(R.J=S.J)} = \frac{1}{NK_{R.J}}`$
        + Equi join m:n foreign key e foreign key: $`f_{p(R.J=S.J)} = \frac{1}{NK_{T.J}}`$ (T.J Primary key)
    * $`f_{q(R,S)}`$: fattore di selettività di join con predicati locali
    * $`ER_{qR,s}`$ (Expected records from $`q_{R,S}`$) = $`f_{q R,S} \times NR_{R} \times NR_{S}`$
    * $`ER_{FJ(R,s)}`$ (Expected records from join senza predicati locali) = $`f_{FJ(R,S)} \times NR_{R} \times NR_{S}`$