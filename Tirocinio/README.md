# Tirocinio

### Istruzioni

- Per il tirocinio non sono validi gli impieghi di didattica (non puoi insegnare)

- Trovare l'azienda con cui fare il tirocinio: consulta sulla tua pagina personale la sezione tirocinio, li troverai tutte le aziende affiliate. Una volta che hai trovato quella giusta scrivi al responsabile e fai domanda di applicazione sulla piattaforma (nel campo "cosa farò" dai una spiegazione abbastanza esaustiva)

- Ho già lavorato, riconoscetemi quello come tirocinio: non è necessario che l'azienda sia affiliata, basta che ti fai fare un attestato dal tuo capo

- Ogni giorno in azienda devi compilare un diario in cui scirvi ciò che hai fatto

- Quando hai finito il tirocinio fai una relazione (almeno 5/6 pagine)

- Quando hai fatto e caricato tutto rompi il cazzo al tuo tutor (il tutor che ti assegneranno) perchè lui approvi il tuo lavoro

- Quando il tutor valida tutto puoi iscriverti all'appello virtuale del responsabile per la verbalizzazione, se hai fretta rompi il cazzo al responsabile