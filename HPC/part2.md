### Part 2

- Von Neumann architecture, standard

    ![alt text](./res/neumann-arch.png "Von Neumann")

- Abstract Parallel Architecture
    
    ![alt text](./res/ab-par-ar.png "Parallel Arch")

    - There is no "typical" parallel computer: different vendors use different architectures
        - Parallel programs must be tailored to the underlying parallel architecture
- How to limit the bottlenecks of the Von Neumann architecture
    - Reduce the memory access latency
        - Rely on CPU registers whenever possible
        - Use caches
            - Cache memory is very fast
            - Often located inside the processor chip
            - Expensive, very small compared to system memory
            - Contains a copy of the content of some recently accessed (main) memory locations
                - If the same memory location is accessed again, and its content is in cache, then the cache is accessed instead of the system RAM
                - If the CPU accesses a memory location whose content is not already in cache the content of that memory location and "some" adjacent locations are copied in cache
            - The smallest unit of data that can be transferred to/from the cache is the cache line (usually 64/128B)
            - Cache memory works well when applications exhibit spatial and/or temporal locality
                - Spatial locality: accessing adjacent memory locations
                - Temporal locality: repeatedly accessing the same memory location(s)
            - Example (matrix-matrix product, given two square matrices p, q, compute r = p x q)
                - Sequential algorithm
                    ```c
                    void matmul( double *p, double* q, double *r, int n) {
                        int i, j, k;
                        for (i=0; i<n; i++) {
                            for (j=0; j<n; j++) {
                                double s = 0.0;
                                for (k=0; k<n; k++) {
                                    s += p[i*n + k] * q[k*n + j];
                                }
                                r[i*n + j] = s;
                            }
                        }
                    }
                    ```
                - Matrix representation

                    ![alt text](./res/mat-rep.png "Matrix representation")

                - Row-wise vs column-wise access

                    ![alt text](./res/rw-cw.png "Matrix representation 2")

                - Optimizing the memory access pattern

                    ![alt text](./res/mem-ac-op.png "Matrix optimization")
            
            - False sharing: may happen when different cores update different data items that fall within the same cache line
                - Write thru: write to cache and immediately to RAM
                - Write back: when cache line is deleted write its content to RAM
                - Write invalidate: when a core writes a cache line into RAM, if another core is editing the same cache line the second cache line is invalidated
                - Example
                    ```c
                    /* A cache line can hold 8 integers */

                    #define NITER 10000000
                    int a[NCORES]; /* shared array, NCORES = 8 */
                    
                    /* Core k does this */
                    for (i=0; i<NITER; i++) {
                        a[k] += f(k);
                    }
                    ```

                    ![alt text](./res/fal-sha1.png "Cache line 1")

                    - Avoiding false sharing (doing this is rare, usually this is done at an higher level)
                        ```c
                        #define NITER 10000000
                        #define PAD 16 /* Padding that fills a cache line */

                        int a[NCORES][PAD]; /* shared matrix */

                        /* Core k does this */
                        for (i=0; i<NITER; i++) {
                            a[k][0] += f(k);
                        }
                        ```

                        ![alt text](./res/fal-sha2.png "Cache lines")

    - Hide the memory access latency
        - Multithreading and context-switch during memory accesses (allows the CPU to switch to another task when the current task is stalled)
            - Fine-grained multithreading
                - A context switch has essentially zero cost
                - Requires CPU with specific support
                - Simultaneous multithreading (SMT): implementation where different threads can use multiple functional units at the same time
                    - HyperThreading is Intel's implementation of SMT (lscpu to see info about CPU)
                        - Each physical processor core is seen by the Operating System as two "logical" processors 
                        - Each “logical” processor maintains a complete set of the architecture state
                            - general-purpose registers
                            - control registers
                            - advanced programmable interrupt controller (APIC) registers
                            - some machine state registers
                        - How it works?
                            - The pipeline stages are separated by two buffers (one for each executing thread)
                            - If one thread is stalled, the other one might go ahead and fill the pipeline slot, resulting in higher processor utilization
            - Coarse-grained multithreading
                - A context switch has non-negligible cost, and is appropriate only for threads blocked on I/O operations or similar
                - The CPU is less efficient in the presence of stalls of short duration
            
    - Execute multiple instructions at the same time
        - Pipelining and multiple issue
            - Pipelining: the functional units are organized like an assembly line, and can be used strictly in that order
            - Multiple issue: the functional units can be used whenever required
                - Static: the order in which functional units are activated is decided at compile time
                - Dynamic: the order in which functional units are activated is decided at run time
            
            ![alt text](./res/pip-mult-is.png "Pipelining and multiple issue")

            - Control dependency (can limit the performance of pipelined architectures, so..)
                ```c
                z = x + y;
                if ( z > 0 ) {
                    w = x ;
                } else {
                    w = y ;
                }

                // BECOMES

                z = x + y;
                c = z > 0;
                w = x*c + y*(1-c);

                // DON'T DO THIS BY HAND, LET THE COMPILER DO HIS JOB
                ```

        - Branch prediction
        - Speculative execution
        - SIMD extensions
            - SSE (Streaming SIMD Extensions), explained below
- Flynn's Taxonomy

    ![alt text](./res/fly-tax.png "Flynn's Taxonomy")

    - SIMD: SIMD instructions apply the same operation (e.g., sum, product...) to multiple elements (typically 4 or 8, depending on the width of SIMD registers and the data types of operands), this means that there must be 4/8/... independent ALUs
        - SSE (Streaming SIMD Extensions)
            - Extension to the x86 instruction set
            - Provide new SIMD instructions operating on small arrays of integer or floating-point numbers
            - Introduces 8 new 128-bit registers (XMM0—XMM7)
            - SSE2 instructions can handle (or)
                - 2 64-bit doubles
                - 2 64-bit integers
                - 4 32-bit integers
                - 8 16-bit integers
                - 16 8-bit chars
            - Example
                ```c
                __m128 a = _mm_set_ps( 1.0, 2.0, 3.0, 4.0 );
                __m128 b = _mm_set_ps( 2.0, 4.0, 6.0, 8.0 );
                __m128 ab = _mm_mul_ps(a, b);
                ```

                ![alt text](./res/sse2-ex.png "Example of SSE")

    - MIMD: multiple execution units that can execute multiple sequences of instructions
        - Architectures
            - Shared Memory: set of processors sharing a common memory space
            - Distributed Memory: set of compute nodes connected through an interconnection network
            - Hybrid: shared + distributed
- Remember young stoner:
    - Computation is fast
    - Communication is slow
    - Input/output is incredibly slow
