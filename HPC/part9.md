### Part 9

- SIMD (Single Instruction Multiple Data): same operation can be applied to multiple data items
    - Powerful but difficult
        - Compilers are not very smart at auto-vectorization
        - Using SIMD instructions by hand is cumbersome, error-prone and non-portable (each processor has its own set of SIMD instructions)
    - Registers in different machines
        - Intel SSE/AVX
            - Registers
                - SSE: 16 * 128-bit SIMD registers XMM0—XMM15 (in x86-64 mode)
                - AVX2: 16 * 256-bit SIMD registers YMM0—YMM15
                - AVX-512: 32 * 512-bit SIMD registers ZMM0—ZMM31
            - Types

                ![alt text](./res/ssety.png "SSE/AVX types")

                ![alt text](./res/avx2ty.png "AVX2 types")

        - ARM NEON
            - Registers
                - 32 64-bit registers that can be seen as
                    - 16 * 128-bit quadword registers Q0—Q15
                    - 32 * 64-bit doubleword registers D0—D31
            - Types

                ![alt text](./res/neonty.png "ARM NEON types")

    - Checking for SIMD support on Linux: `cat /proc/cpuinfo`
    - Vectorization (parallelization)
        ```c
        float vsum(float *v, int n){
            float vs[4] = {0.0, 0.0, 0.0, 0.0};
            float s = 0.0;
            int i;
            for (i=0; i<n-4; i += 4) {
                vs[0] += v[i  ];
                vs[1] += v[i+1];
                vs[2] += v[i+2];
                vs[3] += v[i+3];
            }
            s = vs[0] + vs[1] + vs[2] + vs[3];
            /* Handle leftover */
            for ( ; i<n; i++) {
                s += v[i];
            }
            return s;
        }
        ```

        ![alt text](./res/vect1.png "Vectorization")

        ![alt text](./res/vect2.png "Vectorization")

        ![alt text](./res/vect3.png "Vectorization")

        ![alt text](./res/vect4.png "Vectorization")

        ![alt text](./res/vect5.png "Vectorization")

        - If the array length isn't multiple of SIMD vector length?
            - Padding: add “dummy” elements at the beginning/end to make the array length multiple of SIMD vector length
            - Handle the leftovers with scalar operations
    - Auto-vectorization (auto-parallelization)
        - GCC flags
            - `-O2`: turn on optimization
            - `-ftree-vectorize`
            - `-fopt-info-vec-optimized` and `-fopt-info-vec-missed`: debugging output (what gets vectorized and what not)
            - `-march=native`: autodetect and use SIMD instructions available on your platform
        - Compilation
            - Success: `note: loop vectorized`
            - Insuccess: `note: not vectorized: unsupported use in stmt`
            - Possible errors: `note: reduction: unsafe fp math optimization`
                - FP addition is not associative: (a + b) + c could produce a different result than a + (b + c); therefore, GCC by default does not apply optimizations that violate FP safety
                - We can tell GCC to optimize anyway with `-funsafe-math-optimizations`
            - Example: [simd-vsum-auto.c](./code/simd-vsum-auto.c)
    - Vector data types: non-portable, compiler-specific extensions
        - What is this? 
            - Small vectors of some numeric type (`char, int, float, double`)
            - Ordinary arithmetic operations (sum, product...) can be applied to vector data types
            - The compiler emits the appropriate SIMD instructions for the target architecture if available, if not the compiler emits equivalent scalar code
            - Machine-independent but compiler-dependent
        - Definition
            - v4i is a vector of elements of type int; variables of type v4i occupy 16 bytes of contiguous memory: `typedef int v4i __attribute__((vector_size(16)));`
            - v4f is a vector of elements of type float; variables of type v4f occupy 16 bytes of contiguous memory: `typedef float v4f __attribute__((vector_size(16)));`
            - v8f is a vector of elements of type float; variables of type v8f occupy 32 bytes of contiguous memory: `typedef float v8f __attribute__((vector_size(32)));`
            - Length (num. of elements) of v4f: `#define VLEN (sizeof(v4f)/sizeof(float))`
            - If the target architecture does not support SIMD registers of the specified length, the compiler takes care of that
            - Example: [simd-vsum-vector.c](./code/simd-vsum-vector.c)
        - Operators
            - Allowed operators: `+, -, *, /, unary minus, ^, |, &, ~, %`
            - NB: `%` is VERY BAD
            - Tricks and troubleshooting
                ```c
                typedef int v4i __attribute__ ((vector_size (16)));
                v4i a, b, c;
                long x;
                a = b + 1; /* OK: a = b + {1,1,1,1}; */
                a = 2 * b; /* OK: a = {2,2,2,2} * b; */
                a = 0; /* Error: conversion not allowed */
                a = x + a; /* Error: cannot convert long to int */
                ```
            - Comparison operators: `=, !=, <, <=, >, >=`
                - Vectors are compared element-wise
                    ```c
                    typedef int v4i __attribute__ ((vector_size (16)));
                    v4i a = {1, 2, 3, 4};
                    v4i b = {3, 2, 1, 4};
                    v4i c;
                    c = (a > b);      /* Result {0, 0,-1, 0}  */
                    c = (a == b);     /* Result {0,-1, 0,-1}  */
                    ```
        - Memory alignment: `malloc()` may or may not return a pointer that is properly aligned
            - Solution
                - For data on the stack
                    - `__BIGGEST_ALIGNMENT__` is 16 for SSE, 32 for AVX; it is therefore the preferred choice since it is automatically defined to suit the target: `float v[1024] __attribute__((aligned(__BIGGEST_ALIGNMENT__)));`
                - For data on the heap  
                    - Compile with: `-D_XOPEN_SOURCE=600`
                        ```c
                        float *v;
                        posix_memalign(&v, __BIGGEST_ALIGNMENT__, 1024);
                        // 1024 is the number of bytes to allocate
                        ```
        - Parallelizing if, for, while...
            - Scalar
                ```c
                int a[4] = { 12, -7, 2,  3 };
                int i;
                for (i=0; i<4; i++) {
                    if ( a[i] > 0 ) {
                        a[i] = 2;
                    } else {
                        a[i] = 1;
                    }
                }
                ```
            - Parallel
                ```c
                v4i a = { 12, -7, 2,  3 };
                v4i vtrue = {2, 2, 2, 2};
                v4i vfalse = {1, 1, 1, 1};
                v4i mask = (a > 0); /* mask = {-1, 0, -1, -1} */
                a = (mask & vtrue) | (~mask & vfalse);
                ```

                ![alt text](./res/simdif.png "Parallelizing if, for, while..")

        - Data layout
            - Arrays of Structures (AoS)
                ```c
                typedef point3d {
                    float x, y, z, m;
                };
                point3d *particles;
                ```

                ![alt text](./res/aos.png "Arrays of structures")

            - Structures of Arrays (SoA)
                ```c
                typedef struct points3d {
                    float *x;
                    float *y;
                    float *z;
                    float *m;
                };
                points3d particles;
                ```

                ![alt text](./res/soa.png "Structures of arrays")

    - SIMD instrinsics ([Intel Intrinsics Guide](https://software.intel.com/sites/landingpage/IntrinsicsGuide/)) (NON IN PROGRAMMA)
        - The compiler exposes all low-level SIMD operations through C functions; each function maps to the corresponding low-level SIMD instruction
        - Machine-dependent but compiler-independent
        - SSE intrinsics ([Vector sum](./code/simd-vsum-intrinsics.c))
            - Data types
                - `__m128`: Four single-precision floats
                - `__m128d`: Two double-precision floats
                - `__m128i`
                    - Two 64-bit integers
                    - Four 32-bit integers
                    - Eight 16-bit integers
                    - Sixteen 8-bit integers
            - Memory operations
                - `__m128 _mm_loadu_ps(float *aPtr)`: load 4 floats starting from memory address aPtr (unaligned)
                - `__m128 _mm_load_ps(float *aPtr)`: load 4 floats starting from memory address aPtr, aPtr must be a multiple of 16
                - `_mm_storeu_ps(float *aPtr, __m128 v)`: store 4 floats from v to memory address aPtr (unaligned)
                - `_mm_store_ps(float *aPtr, __m128 v)`: store 4 floats from v to memory address aPtr, aPtr must be a multiple of 16
                - ...
            - How values are stored in memory

                ![alt text](./res/simdmem.png "How SIMD values are stored in memory")

            - Arithmetic operations
                - Do operation `<op>` on `a` and `b`, write result to `c`: `__m128 c = _mm_<op>_ps(a, b);`
                    - `add`
                    - `sub`
                    - `mul`
                    - `div`
                    - `min`
                    - `max`
                    - `sqrt`
                    - ...
            - Load constants
                
                ![alt text](./res/simdcon.png "Load constants SIMD")