### Part 6

- OpenMP
    - Info
        - Model for shared-memory parallel programming
        - Portable across shared-memory architectures
        - Incremental parallelization: parallelize individual computations in a program while leaving the rest of the program sequential
        - Compiler generates thread programs and synchronization (Compatible: Fortran, C/C++)
    - Main constructs
        - `#pragma omp parallel`: parallel region
        - `#pragma omp parallel num_threads(thr)`: parallel region with thread number specification
        - `#pragma omp parallel if(num_thr>=4) num_threads(num_thr)`
            - True: parallel construct is enabled with num_thr threads
            - False: parallel construct is ignored
        - `int omp_get_thread_num()`: returns the number of the caller thread
        - `int omp_get_num_threads()`: returns the number of threads in the currently active pool
        - `int omp_get_max_threads()`: returns the "default value" of threads number
        - `omp_set_num_threads(4)`: sets the number of threads to 4
        - `double omp_get_wtime()`: taking times
            ```c
            double tstart, tstop;
            tstart = omp_get_wtime();
            #pragma omp ... 
                {/* block of code to measure */}
            tstop = omp_get_wtime();
            printf("Elapsed time: %f\n", tstop - tstart);
            ```
        - `env OMP_NUM_THREADS N` and `export OMP_NUM_THREADS=N`: set the default number of threads with an environment variable
        - `#pragma omp barrier`: sync barrier (often specified by default); all threads in the currently active team must reach this point before they are allowed to proceed
        - `#pragma omp critical`: critical section of code; all threads will eventually execute the critical section, however, only one thread at a time can be inside the critical block
        - `#pragma omp atomic`: protects updates to a shared variable (ensures that only one thread at the time updates a shared variable)
        - `#pragma omp for`: used inside a parallel block, loop iterations are assigned to the threads of the current team (the ones created with omp parallel) and the loop variable is made private by default
            - It can be collapsed in: `#pragma omp parallel for`
            - Legal forms

                ![alt text](./res/legfor.png "Lefal forms of omp parallel")

                - Variable index must have integer or pointer type
                - The expressions start, end, and incr must have a compatible type and must not change during execution of the loop
                - Variable index can only be modified by the “increment expression” in the “for” statement
            - It is not possible to use a parallel for directive if data dependencies are present
                - To check this here's a dirty hack: run the loop backwards; if the program is still correct, the loop might be parallelizable
            - `schedule(type, chunksize)`: task scheduling
                - `type`
                    - `static`: the iterations can be assigned to the threads before the loop is executed; if `chunksize` is not specified, iterations are evenly divided contiguously among threads
                    - `dynamic` or `guided`: iterations are assigned to threads while the loop is executing (`guided`: from bigger to smaller chunk)
                    - `auto`: the compiler and/or the run-time system determines the schedule
                    - `runtime`: the schedule is determined at run-time using the OMP_SCHEDULE environment variable
                - `chunksize`: to be chosen by trial and error
            - `collapse`: specifies how many loops in a nested loop should be collapsed into one large iteration space and divided according to the schedule clause
                - Counters are made private by default
                - How it works?
                    - The for matrix is "rolled out"

                        ![alt text](./res/exrolledout.png "Parallel trapezoid rule")

            - Usage example: [Serial Odd-Even Transposition Sort](./code/omp-odd-even.c)
                - Compare all (even, odd) pairs of adjacent elements, and exchange them if in the wrong order
                - Then compare all (odd, even) pairs, exchanging if necessary; repeat the step above
                - How it works?
                    
                    ![alt text](./res/serialodd.png "Serial Odd-Even Transposition Sort")
        
        - `sections`: specifies that the enclosed section(s) of code are to be divided among the threads in the team
            - `sections` -> `section` (0..n)
            - Each section is executed once 
            - Different section may be executed by different threads
            - A thread might execute more than one section if it is quick enough and the implementation allows that
            - There is an implied barrier at the end of a sections directive, unless the `nowait` clause is used
        - `#pragma omp parallel reduction(<op>:<variable>)`
            - `op`: +, -, *, |, ^, &, |, &&, ||
            - Works with the "neutral" value relative to `op`

            ![alt text](./res/reduction.png "Reduction example")

        - `#pragma omp master`: marks a parallel region which is executed by the master only (ID = 0), other threads just skip the region; there is no implicit barrier at the end of the block
            
            ![alt text](./res/exmaster.png "Example of master rule")

        - `#pragma omp single`: marks a parallel region which is executed once by the first thread reaching it, whichever it is; a barrier is implied at the end of the block
        - `#pragma omp task`

            ![alt text](./res/omptask.png "Example of task rule")

            - Task vs Section
                - Sections must be completed (unless the nowait clause is used) inside the sections block
                - Tasks are queued and completed whenever possible
            - Most used data scope (see next for more info about scopes): `firstprivate`
                - Detail and example

                    ![alt text](./res/taskds.png "Data scoping with tasks")

                - Linked list traversal with tasks

                    ![alt text](./res/taskllt.png "Linked list traversal with tasks")

            - Tasks syncing is done
                - At thread barriers (explicit or implicit)
                - `#pragma omp taskwait`: wait until all tasks defined in the current task (not in the descendants!) have completed

                    ![alt text](./res/taskwaitex.png "taskwait directive example")

            - Example: [Recursive Fibonacci](./code/omp-fibonacci-tasks.c)     
    - Execution model

        ![alt text](./res/omp-ex-mod.png "OpenMP execution model") 

    - Pragmas: special preprocessor instructions
        - Compilers that don’t support the pragmas ignore them
        - Pragmas are used in OpenMP to tell the preprocessor to parallelize the next portion of code
            - In those portions of code there must be one point of entry and one point of exit (so for example instructions like `exit`, `return`, `break`, `continue`... can't be inserted in those pieces of code)
        - How it works
            - `#pragma omp parallel`
                - When a thread reaches a parallel directive, it creates a pool of threads and becomes the master of the team (ID = 0)
                - The default pool size is equal to the number of logic cores
                - NB: The code of the parallel region is duplicated and all threads will execute it
                - Examples
                    - [Hello, world](./code/omp-demo0.c)
                    - [Say hello](./code/omp-demo2.c)
                    - [Max threads and Num threads](./code/omp-demo4.c)
    - Nested parallelism (won't be used)
        - Nesting can be enabled (`OMP_NESTED=true`); if it's not enabed the nested `pragma` will be ignored
        - `omp_get_num_threads()` returns the number of the innermost thread pool a thread is part of
        - `omp_get_thread_num()` returns the ID of a thread in the innermost pool it is part of

        ![alt text](./res/nest.png "Nested parallelism scheme")
    - Scope
        - In OpenMP the scope of a variable refers to the set of threads that can access the variable
        - Default
            - All variables that are visible at the beginning of a parallel block are shared across threads
            - All variables defined inside a parallel block are private to each thread
        - Setting the scope
            - `shared(x)`: all threads access the same memory location
            - `private(x)`
                - Each thread has its own private copy of x
                - All local instances of x are not initialized
                - Local updates to x are lost when exiting the parallel region
                - The original value of x is retained at the end of the block 
            - `firstprivate(x)`: same as `private(x)` but local instances are initialized
            - `default(shared)` or `default(none)`
                - Affects all the variables not specified in other clauses
                - `default(none)`: ensures that you must specify the scope of each variable used in the parallel block
                    - In GCC < 9.x const variables are predetermined shared whereas in GCC ≥ 9.x you must explicitly specify the visibility of every variable
                        - Solution (correct in all versions)
                            ```c
                            const int foo = 1;
                            #pragma omp parallel default(none) firstprivate(foo)
                                {int baz = 0;baz += foo; }
                            ```
    - Example: the trapezoid rule
        - [Serial version](./code/trap.c)
        
            ![alt text](./res/trap-rule.png "The trapezoid rule")

        - Parallel version

            ![alt text](./res/trap-par.png "Parallel trapezoid rule")

            - [Version 1](./code/omp-trap0.c)
                - Tasks
                    - Split the n intervals across OpenMP threads
                    - Thread t stores its result in partial_result[t]
                    - The master sums all partial results
            - [Version 2](./code/omp-trap1.c)
                - Tasks
                    - Split the n intervals across OpenMP threads
                    - Thread t stores its result in a local variable partial_result and updates the global result
                    - The master sums all partial results
            - [Version 3](./code/omp-trap2.c): same as Version 2 but with reduction